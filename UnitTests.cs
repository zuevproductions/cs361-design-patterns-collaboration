﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculationLibrary1;




namespace UnitTestProject1
{
    /// <summary>
    /// Сводное описание для UnitTests
    /// </summary>
    [TestClass]
    public class UnitTests
    {
        public UnitTests()
        {
            //
            // TODO: добавьте здесь логику конструктора
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Дополнительные атрибуты тестирования
        //
        // При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        // ClassInitialize используется для выполнения кода до запуска первого теста в классе
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // TestInitialize используется для выполнения кода перед запуском каждого теста 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // TestCleanup используется для выполнения кода после завершения каждого теста
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {
            //
            // TODO: добавьте здесь логику теста
            //
        }

        //тестируем для десятичной

        //константы для результатов
        public const int DecSumTestResult1 = 7;
        public const int DecSumTestResult2 = 0;
        public const int DecSumTestResult3 = 10;

        [TestMethod]
        public void DecSumTest1()
        {

            //arrange

            DecCalculate dst1 = new DecCalculate();


            //act

            string result = dst1.CalculatorOperation("2", "5", 1);

            //assert

            Assert.AreEqual(result, Convert.ToInt32(DecSumTestResult1));

        }

        [TestMethod]
        public void DecSumTest2()
        {

            //arrange

            DecCalculate dst2 = new DecCalculate();


            //act

            string result = dst2.CalculatorOperation("0", "0", 1);

            //assert

            Assert.AreEqual(result, Convert.ToInt32(DecSumTestResult2));

        }


        [TestMethod]
        public void DecSumTest3()
        {

            //arrange

            DecCalculate dst3 = new DecCalculate();


            //act

            string result = dst3.CalculatorOperation("6", "4", 1);

            //assert

            Assert.AreEqual(result, Convert.ToInt32(DecSumTestResult3));

        }

        //константы для результатов
        public const int DecSubTestResult1 = 2;
        public const int DecSubTestResult2 = 0;
        public const int DecSubTestResult3 = 7;



        [TestMethod]
        public void DecSubTest1()
        {
            //arrange

            DecCalculate dsubt1 = new DecCalculate();


            //act

            string result = dsubt1.CalculatorOperation("6", "4", 2);

            //assert

            Assert.AreEqual(result, Convert.ToInt32(DecSubTestResult1));
        }

        [TestMethod]
        public void DecSubTest2()
        {
            //arrange

            DecCalculate dsubt2 = new DecCalculate();


            //act

            string result = dsubt2.CalculatorOperation("5", "5", 2);

            //assert

            Assert.AreEqual(result, Convert.ToInt32(DecSubTestResult2));
        }

        [TestMethod]
        public void DecSubTest3()
        {
            //arrange

            DecCalculate dsubt3 = new DecCalculate();


            //act

            string result = dsubt3.CalculatorOperation("7", "0", 2);

            //assert

            Assert.AreEqual(result, Convert.ToInt32(DecSubTestResult3));
        }



        [TestMethod]
        public void ConvertFromDec1()
        {

            //arrange

            BinCalculate bc = new BinCalculate();


            //act

            string result = bc.CalculatorOperation("5", "0", 1);


            //assert

            Assert.AreEqual(Convert.ToInt32(result, 2), Convert.ToInt32(5, 2));

        }

        public void ConvertFromDec2()
        {

            //arrange

            BinCalculate bc = new BinCalculate();


            //act

            string result = bc.CalculatorOperation("555", "0", 1);


            //assert

            Assert.AreEqual(Convert.ToInt32(result, 2), Convert.ToInt32("555"));

        }

        [TestMethod]
        public void ConvertFromDec3()
        {

            //arrange

            BinCalculate dc = new DexCalculate();


            //act

            string result = dc.CalculatorOperation("9", "0", 1);


            //assert

            Assert.AreEqual(Convert.ToInt32(result, 16), Convert.ToInt32(9, 16));

        }

        [TestMethod]
        public void ConverToDec4()
        {

            //arrange

            BinCalculate bc = new BinCalculate();


            //act

            string result = bc.CalculatorOperation("101", "0", 1);


            //assert

            Assert.AreEqual(Convert.ToInt32(result, 10), Convert.ToInt32("5", 10));

        }

        public void ConvertToDec5()
        {

            //arrange

            BinCalculate bc = new BinCalculate();


            //act

            string result = bc.CalculatorOperation("23", "0", 1);


            //assert

            Assert.AreEqual(Convert.ToInt32(result, 10), Convert.ToInt32("23", 10));

        }

        [TestMethod]
        public void ConvertFromDec6()
        {

            //arrange

            DexCalculate dc = new DexCalculate();


            //act

            string result = dc.CalculatorOperation("123", "0", 1);


            //assert

            Assert.AreEqual(Convert.ToInt32(result, 16), Convert.ToInt32(123, 16));

        }

        [TestMethod]
        public void ConvertToDec3()
        {

            //arrange

            DexCalculate dc = new DexCalculate();


            //act

            string result = dc.CalculatorOperation("100", "0", 1);


            //assert

            Assert.AreEqual(Convert.ToInt32(result, 10), Convert.ToInt32("256", 16));

        }



        //тестируем для двоичной

        //константы для результатов
        public const string BinSumTestResult1 = "1";
        public const string BinSumTestResult2 = "101";
        public const string BinSumTestResult3 = "100100";

        [TestMethod]
        public void BinSumTest1()
        {

            //arrange

            BinCalculate bst1 = new BinCalculate();


            //act

            string result = bst1.CalculatorOperation("1", "0", 1);

            //assert

            Assert.AreEqual(result, BinSumTestResult1);

        }

        [TestMethod]
        public void BinSumTest2()
        {

            //arrange

            BinCalculate bst2 = new BinCalculate();


            //act

            string result = bst2.CalculatorOperation("100", "1", 1);

            //assert

            Assert.AreEqual(result, BinSumTestResult2);

        }


        [TestMethod]
        public void BinSumTest3()
        {

            //arrange

            BinCalculate bst3 = new BinCalculate();


            //act

            string result = bst3.CalculatorOperation("100000", "100", 1);

            //assert

            Assert.AreEqual(result, BinSumTestResult3);

        }

        //константы для результатов
        public const string BinSubTestResult1 = "101";
        public const string BinSubTestResult2 = "111101";
        public const string BinSubTestResult3 = "0";



        [TestMethod]
        public void BinSubTest1()
        {
            //arrange

            BinCalculate bsubt1 = new BinCalculate();


            //act

            string result = bsubt1.CalculatorOperation("111", "10", 2);

            //assert

            Assert.AreEqual(result, BinSubTestResult1);
        }

        [TestMethod]
        public void BinSubTest2()
        {
            //arrange

            BinCalculate bsubt2 = new BinCalculate();


            //act

            string result = bsubt2.CalculatorOperation("111101", "10", 2);

            //assert

            Assert.AreEqual(result, BinSubTestResult2);
        }

        [TestMethod]
        public void BinSubTest3()
        {
            //arrange

            BinCalculate bsubt3 = new BinCalculate();


            //act

            string result = bsubt3.CalculatorOperation("111", "111", 2);

            //assert

            Assert.AreEqual(result, BinSubTestResult3);
        }

        [TestMethod]
        public void BinConvertTest1()
        {

            //arrange
            BinCalculate b = new BinCalculate();

            //act
            string result = b.CalculatorOperation("111111","0",1);

            //assert

            Assert.AreEqual(result, "111111");
        }

        [TestMethod]
        public void BinConvertTest2()
        {


            //arrange
            BinCalculate b = new BinCalculate();

            //act
            string result = b.CalculatorOperation("1", "0", 1);

            //assert

            Assert.AreEqual(result, "1");
        }

        [TestMethod]
        public void BinConvertTest3()
        {
            //arrange
            DecCalculate d = new DecCalculate();

            //act
            string result = d.CalculatorOperation();

            //assert
            Assert.AreEqual(result, "");

        }

        [TestMethod]
        public void BinConvertTest4()
        {
            //arrange
            DexCalculate d = new DexCalculate();

            //act
            string result = d.CalculatorOperation();

            //assert
            Assert.AreEqual(result, "");

        }

        //тесты для шестнадцатиричной

        [TestMethod]
        public void DexTest1()
        {
            DexCalculate d = new DexCalculate();
            string result d.CalculatorOperation("A","1",1);
            Assert.AreEqual(result,"B");
        }

        [TestMethod]
        public void DexTest2()
        {
            DexCalculate d = new DexCalculate();
            string result d.CalculatorOperation("100","100",1);
            Assert.AreEqual(Convert.ToInt32(result,16),Convert.ToInt32("512",10));
        }

        [TestMethod]
        public void DexTest3()
        {
            DexCalculate d = new DexCalculate();
            string result d.CalculatorOperation("A","1",2);
            Assert.AreEqual(result,"9");
        }

        [TestMethod]
        public void DexTest4()
        {
            DexCalculate d = new DexCalculate();
            string result d.CalculatorOperation("100","100",2);
            Assert.AreEqual(Convert.ToInt32(result,16),Convert.ToInt32("0",10));
        }
        }
    /*
        [TestMethod]
        public void DexTest()
        {

        }

        [TestMethod]
        public void DexTest()
        {

        }

        [TestMethod]
        public void DexTest()
        {

        }

        [TestMethod]
        public void DexTest()
        {

        }

        [TestMethod]
        public void DexTest()
        {

        }

        [TestMethod]
        public void DexTest()
        {

        }
    */
    //тестируем восьмеричную
        [TestMethod]
        public void OctalTest1()
        {
            OctalCalculate o = new OctalCalculate();
            string result o.CalculatorOperation("3","5",1);
            Assert.AreEqual(Convert.ToInt32(result,8),Convert.ToInt32("8",10));
        }

        [TestMethod]
        public void OctalTest2()
        {
            OctalCalculate o = new OctalCalculate();
            string result o.CalculatorOperation("143","1",1);
            Assert.AreEqual(Convert.ToInt32(result,8),Convert.ToInt32("100",10));
        }

        [TestMethod]
        public void OctalTest3()
        {
            OctalCalculate o = new OctalCalculate();
            string result o.CalculatorOperation("7","5",2);
            Assert.AreEqual(Convert.ToInt32(result,8),Convert.ToInt32("2",10));
        }

        [TestMethod]
        public void OctalTest4()
        {
            OctalCalculate o = new OctalCalculate();
            string result o.CalculatorOperation("144","1",2);
            Assert.AreEqual(Convert.ToInt32(result,8),Convert.ToInt32("99",10));
        }

    }
}
