﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using CalculationLibrary1.Calculated;

namespace multysystemCalculation
{ 
    public partial class Form1 : Form
    {
        String old="0";
        int opr;
        public Form1()
        {
            InitializeComponent();
            radioButtonOpt.Checked = false;
            radioButtonBinary.Checked = false;
            radioButtonDex.Checked = false;
            radioButtonDec.Checked = true;
            this.Text = "Десятичная";
            r = radioButtonDec;
            

            button4.Enabled = true;
            button5.Enabled = true;
            button6.Enabled = true;

            button7.Enabled = true;
            button8.Enabled = true;
            button9.Enabled = true;

            button15.Enabled = true;
            button14.Enabled = true;
            button13.Enabled = true;

            button11.Enabled = false;
            button10.Enabled = false;
            button18.Enabled = false;

            button17.Enabled = false;
            button16.Enabled = false;
            button21.Enabled = false;

            button18.Enabled = false;
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            old = textBox1.Text;
            opr = 1;
            textBox1.Text = "0";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            old = textBox1.Text;
            opr = 2;
            textBox1.Text = "0";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(opr==1 | opr==2){
                CalculationLibrary1.Calculated.CalculatorOperation c = null;
                if (radioButtonDec.Checked) { c = (CalculationLibrary1.Calculated.CalculatorOperation)new CalculationLibrary1.Calculated.DecCalculate(); }
                if (radioButtonDex.Checked) { c = (CalculationLibrary1.Calculated.CalculatorOperation)new CalculationLibrary1.Calculated.DexCalculate(); }
                if (radioButtonBinary.Checked) { c = (CalculationLibrary1.Calculated.CalculatorOperation)new CalculationLibrary1.Calculated.BinCalculate(); }
                if (radioButtonOpt.Checked) { c = (CalculationLibrary1.Calculated.CalculatorOperation)new CalculationLibrary1.Calculated.OctalCalculate(); }
                textBox1.Text = c.currentOperationCheck(old, textBox1.Text, opr);
            }
            opr = 0;
        }
        void butonoff() {
            button4.Enabled = true;
            button5.Enabled = false;
            button6.Enabled = false;

            button7.Enabled = false;
            button8.Enabled = false;
            button9.Enabled = false;

            button15.Enabled = false;
            button14.Enabled = false;
            button13.Enabled = false;

            button11.Enabled = false;
            button10.Enabled = false;
            button18.Enabled = false;

            button17.Enabled = false;
            button16.Enabled = false;
            button21.Enabled = false;

            button18.Enabled = false;
           
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            textBox1.Text += b.Text;
            if (textBox1.Text.StartsWith("0")) {
                textBox1.Text = textBox1.Text.Substring(1);
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            textBox1.Text = "0";
            old = "";
            opr = 0;
        }

        private void True(object sender, EventArgs e)
        {

        }

        private void radioButtonBinary_Click(object sender, EventArgs e)
        {
            try
            {
                RadioButton b = (RadioButton)sender;
                this.Text = b.Name;
                if (true)
                {
                    string t = textBox1.Text;

                    if (b == radioButtonDec)
                    {
                        this.Text = "Десятичная";
                        if (r == radioButtonDec) { }
                        if (r == radioButtonDex)
                        {
                            t = Convert.ToString(Convert.ToInt64(t, 16), 10);
                            old = Convert.ToString(Convert.ToInt64(old, 16), 10);
                        }
                        if (r == radioButtonBinary)
                        {
                            t = Convert.ToString(Convert.ToInt64(t, 2), 10);
                            old = Convert.ToString(Convert.ToInt64(old, 2), 10);
                        }
                        if (r == radioButtonOpt)
                        {
                            t = Convert.ToString(Convert.ToInt64(t, 8), 10);
                            old = Convert.ToString(Convert.ToInt64(old, 8), 10);
                        }

                        button4.Enabled = true;
                        button5.Enabled = true;
                        button6.Enabled = true;

                        button7.Enabled = true;
                        button8.Enabled = true;
                        button9.Enabled = true;

                        button15.Enabled = true;
                        button14.Enabled = true;
                        button13.Enabled = true;

                        button11.Enabled = false;
                        button10.Enabled = false;
                        button18.Enabled = false;

                        button17.Enabled = false;
                        button16.Enabled = false;
                        button21.Enabled = false;

                        button18.Enabled = false;
                    }
                    if (b == radioButtonDex)
                    {
                        this.Text = "Шестнадцатиричная";
                        if (r == radioButtonDec)
                        {
                            t = Convert.ToString(Convert.ToInt64(t, 10), 16);
                            old = Convert.ToString(Convert.ToInt64(old, 10), 16);
                        }
                        if (r == radioButtonDex)
                        {
                            Console.WriteLine();
                        }
                        if (r == radioButtonBinary)
                        {
                            t = Convert.ToString(Convert.ToInt64(t, 2), 16);
                            old = Convert.ToString(Convert.ToInt64(old, 2), 16);
                        }
                        if (r == radioButtonOpt)
                        {
                            t = Convert.ToString(Convert.ToInt64(t, 8), 16);
                            old = Convert.ToString(Convert.ToInt64(old, 8), 16);
                        }

                        button4.Enabled = true;
                        button5.Enabled = true;
                        button6.Enabled = true;

                        button7.Enabled = true;
                        button8.Enabled = true;
                        button9.Enabled = true;

                        button15.Enabled = true;
                        button14.Enabled = true;
                        button13.Enabled = true;

                        button11.Enabled = true;
                        button10.Enabled = true;
                        button18.Enabled = true;

                        button17.Enabled = true;
                        button16.Enabled = true;
                        button21.Enabled = true;

                        button18.Enabled = true;


                    }
                    if (b == radioButtonBinary)
                    {
                        this.Text = "Бинарная";
                        if (r == radioButtonDec)
                        {
                            t = Convert.ToString(Convert.ToInt64(t, 10), 2);
                            old = Convert.ToString(Convert.ToInt64(old, 10), 2);
                        }
                        if (r == radioButtonDex)
                        {
                            t = Convert.ToString(Convert.ToInt64(t, 16), 2);
                            old = Convert.ToString(Convert.ToInt64(old, 16), 2);
                        }
                        if (r == radioButtonBinary) 
                        {
                            Console.WriteLine();
                        }
                        if (r == radioButtonOpt)
                        {
                            t = Convert.ToString(Convert.ToInt64(t, 8), 2);
                            old = Convert.ToString(Convert.ToInt64(old, 8), 2);
                        }
                        button4.Enabled = true;
                        button5.Enabled = false;
                        button6.Enabled = false;

                        button7.Enabled = false;
                        button8.Enabled = false;
                        button9.Enabled = false;

                        button15.Enabled = false;
                        button14.Enabled = false;
                        button13.Enabled = false;

                        button11.Enabled = false;
                        button10.Enabled = false;
                        button18.Enabled = false;

                        button17.Enabled = false;
                        button16.Enabled = false;
                        button21.Enabled = false;

                        button18.Enabled = false;


                    }
                    if (b == radioButtonOpt)
                    {
                        this.Text = "Восьмиричная";
                        if (r == radioButtonDec)
                        {
                            t = Convert.ToString(Convert.ToInt64(t, 10), 8);
                            old = Convert.ToString(Convert.ToInt64(old, 10), 8);
                        }
                        if (r == radioButtonDex)
                        {
                            t = Convert.ToString(Convert.ToInt64(t, 16), 8);
                            old = Convert.ToString(Convert.ToInt64(old, 16), 8);
                        }
                        if (r == radioButtonBinary)
                        {
                            t = Convert.ToString(Convert.ToInt64(t, 2), 8);
                            old = Convert.ToString(Convert.ToInt64(old, 2), 8);
                        }
                        if (r == radioButtonOpt) { }

                        button4.Enabled = true;
                        button5.Enabled = true;
                        button6.Enabled = true;

                        button7.Enabled = true;
                        button8.Enabled = true;
                        button9.Enabled = true;

                        button15.Enabled = true;
                        button14.Enabled = false;
                        button13.Enabled = false;

                        button11.Enabled = false;
                        button10.Enabled = false;
                        button18.Enabled = false;

                        button17.Enabled = false;
                        button16.Enabled = false;
                        button21.Enabled = false;

                        button18.Enabled = false;
                    }
                    textBox1.Text = t;
                }
                 r = b;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
      
        }
    }
}
