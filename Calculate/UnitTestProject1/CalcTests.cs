﻿using System;
using CalculationLibrary1;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalculatorTests
{
	[TestClass]
	public class CalcTests
	{
		//константы для результатов
		public const int DecSumTestResult1 = 7;
		public const int DecSumTestResult2 = 0;
		public const int DecSumTestResult3 = 10;

		[TestMethod]
		public void DecSumTest1()
		{

			//arrange

            CalculationLibrary1.Calculated.DecCalculate dst1 = new CalculationLibrary1.Calculated.DecCalculate();


			//act

            string result = dst1.currentOperationCheck("2", "5", 1);

			//assert

			Assert.AreEqual (result, DecSumTestResult1);

		}

		[TestMethod]
		public void DecSumTest2()
		{

			//arrange

            CalculationLibrary1.Calculated.DecCalculate dst2 = new CalculationLibrary1.Calculated.DecCalculate();


			//act

            string result = dst2.currentOperationCheck("0", "0", 1);

			//assert

			Assert.AreEqual (result, DecSumTestResult3);

		}


		[TestMethod]
		public void DecSumTest3()
		{

			//arrange

            CalculationLibrary1.Calculated.DecCalculate dst3 = new CalculationLibrary1.Calculated.DecCalculate();


			//act

            string result = dst3.currentOperationCheck ("6", "4", 1);

			//assert

			Assert.AreEqual (result, DecSumTestResult3);

		}

		//константы для результатов
		public const int DecSubTestResult1 = 2;
		public const int DecSubTestResult2 = 0;
		public const int DecSubTestResult3 = 7;



		[TestMethod]
		public void DecSubTest1()
		{
			//arrange

            CalculationLibrary1.Calculated.DecCalculate dsubt1 = new CalculationLibrary1.Calculated.DecCalculate();


			//act

            string result = dsubt1.currentOperationCheck("6", "4", 2);

			//assert

			Assert.AreEqual (result, DecSubTestResult1);
		}

		[TestMethod]
		public void DecSubTest2()
		{
			//arrange

            CalculationLibrary1.Calculated.DecCalculate dsubt2 = new CalculationLibrary1.Calculated.DecCalculate();


			//act

            string result = dsubt2.currentOperationCheck("5", "5", 2);

			//assert

			Assert.AreEqual (result, DecSubTestResult2);
		}

		[TestMethod]
		public void DecSubTest3()
		{
			//arrange

            CalculationLibrary1.Calculated.DecCalculate dsubt3 = new CalculationLibrary1.Calculated.DecCalculate();


			//act

            string result = dsubt3.currentOperationCheck("7", "0", 2);

			//assert

			Assert.AreEqual (result, DecSubTestResult3);
		}


	}
}

