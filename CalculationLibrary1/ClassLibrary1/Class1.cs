﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CalculationLibrary1
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

     namespace Calculated
    {
         public abstract class CalculatorOperation//Calc
        {
            //int Sum(int a, int b);
            // int Res(int a, int b);
            protected abstract string toSysA(int i);
            protected abstract int toInt(string a);
            public string currentOperationCheck(string a, string b, int opr)
            {
                int ai, bi;
                ai = toInt(a);
                bi = toInt(b);
                if (opr == 1)
                {
                    return toSysA(ai + bi);
                }
                if (opr == 2)
                {
                    return toSysA(ai - bi);
                }
                throw new Exception("uncnow opr,opr mast equal \"+\" or \"-\" ");
            }
        }

         public class BinCalculate : CalculatorOperation
        {
            protected override string toSysA(int i)
            {
                return Convert.ToString(i, 2);
            }
            protected override int toInt(string a)
            {
                return Convert.ToInt32(a, 2);
            }
        }
         public class DecCalculate : CalculatorOperation
        {
            protected override string toSysA(int i)
            {
                return Convert.ToString(i, 10);
            }
            protected override int toInt(string a)
            {
                return Convert.ToInt32(a, 10);
            }
        }
         public class DexCalculate : CalculatorOperation
        {
            protected override string toSysA(int i)
            {
                return Convert.ToString(i, 16);
            }
            protected override int toInt(string a)
            {
                return Convert.ToInt32(a, 16);
            }
        }
         public class OctalCalculate : CalculatorOperation
         {
             protected override string toSysA(int i)
             {
                 return Convert.ToString(i, 8);
             }
             protected override int toInt(string a)
             {
                 return Convert.ToInt32(a, 8);
             }
         }



    }

}
