﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Calc;


namespace CulcLocal
{
     internal abstract class Culc
    {
        //int Sum(int a, int b);
       // int Res(int a, int b);
      protected abstract string toSysA(int i);
       protected abstract  int toInt(string a);
         public string  culc(string a,string b,int opr)
        {
            int ai,bi;
            ai=toInt(a);
            bi=toInt(b);
            if(opr==1){
               return toSysA(ai+bi);
            }
            if(opr==2){
             return toSysA(ai-bi);
            }
            throw new Exception("uncnow opr,opr mast equal \"+\" or \"-\" ");
        }
    }

     internal class BinCulc : Culc
     {
        protected override string toSysA(int i)
        {
            return Convert.ToString(i, 2);
        }
        protected override int toInt(string a)
        {
            return Convert.ToInt32(a, 2);
        }
    }
     internal class DecCulc : Culc
    {
        protected override string toSysA(int i)
        {
            return Convert.ToString(i, 10);
        }
        protected override int toInt(string a)
        {
            return Convert.ToInt32(a, 10);
        }
    }
     internal class DexCulc : Culc
    {
        protected override string toSysA(int i)
        {
            return Convert.ToString(i, 16);
        }
        protected override int toInt(string a)
        {
            return Convert.ToInt32(a, 16);
        }
    }


}
