﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ClassLibrary1.Culculated;

namespace Calc
{ 
    public partial class Form1 : Form
    {
        String old;
        int opr;
        public Form1()
        {
            InitializeComponent();
            radioButton3.Checked = false;
            radioButton2.Checked = false;
            radioButton1.Checked = true;
            this.Text = "Десятичная";
            

            button4.Enabled = true;
            button5.Enabled = true;
            button6.Enabled = true;

            button7.Enabled = true;
            button8.Enabled = true;
            button9.Enabled = true;

            button15.Enabled = true;
            button14.Enabled = true;
            button13.Enabled = true;

            button11.Enabled = false;
            button10.Enabled = false;
            button18.Enabled = false;

            button17.Enabled = false;
            button16.Enabled = false;
            button21.Enabled = false;

            button18.Enabled = false;
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                RadioButton b = (RadioButton)sender;
                this.Text = b.Name;
                if (true)
                {
                    string t = textBox1.Text;

                    if (b == radioButton1)
                    {
                        this.Text = "Десятичная";
                        if (r == radioButton1) { }
                        if (r == radioButton2) { t = Convert.ToString(Convert.ToInt64(t, 16), 10); }
                        if (r == radioButton3) { t = Convert.ToString(Convert.ToInt64(t, 2), 10); }

                        button4.Enabled = true;
                        button5.Enabled = true;
                        button6.Enabled = true;

                        button7.Enabled = true;
                        button8.Enabled = true;
                        button9.Enabled = true;

                        button15.Enabled = true;
                        button14.Enabled = true;
                        button13.Enabled = true;

                        button11.Enabled = false;
                        button10.Enabled = false;
                        button18.Enabled = false;

                        button17.Enabled = false;
                        button16.Enabled = false;
                        button21.Enabled = false;

                        button18.Enabled = false;
                    }
                    if (b == radioButton2)
                    {
                        this.Text = "Шестнадцатиричная";
                        if (r == radioButton1) { t = Convert.ToString(Convert.ToInt64(t, 10), 16); }
                        if (r == radioButton2) { }
                        if (r == radioButton3) { t = Convert.ToString(Convert.ToInt64(t, 2), 16); }

                        button4.Enabled = true;
                        button5.Enabled = true;
                        button6.Enabled = true;

                        button7.Enabled = true;
                        button8.Enabled = true;
                        button9.Enabled = true;

                        button15.Enabled = true;
                        button14.Enabled = true;
                        button13.Enabled = true;

                        button11.Enabled = true;
                        button10.Enabled = true;
                        button18.Enabled = true;

                        button17.Enabled = true;
                        button16.Enabled = true;
                        button21.Enabled = true;

                        button18.Enabled = true;
           

                    }
                    if (b == radioButton3)
                    {
                        this.Text = "Бинарная";
                        if (r == radioButton1) { t = Convert.ToString(Convert.ToInt64(t, 10), 2); }
                        if (r == radioButton2) { t = Convert.ToString(Convert.ToInt64(t, 16), 2); }
                        if (r == radioButton3) { }
                        button4.Enabled = true;
                        button5.Enabled = false;
                        button6.Enabled = false;

                        button7.Enabled = false;
                        button8.Enabled = false;
                        button9.Enabled = false;

                        button15.Enabled = false;
                        button14.Enabled = false;
                        button13.Enabled = false;

                        button11.Enabled = false;
                        button10.Enabled = false;
                        button18.Enabled = false;

                        button17.Enabled = false;
                        button16.Enabled = false;
                        button21.Enabled = false;

                        button18.Enabled = false;
           

                    }
                    textBox1.Text = t;
                }
                r = b;
            }catch(Exception ex){}
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            old = textBox1.Text;
            opr = 1;
            textBox1.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            old = textBox1.Text;
            opr = 2;
            textBox1.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(opr==1 | opr==2){
                ClassLibrary1.Culculated.Culc c = null;
            if (radioButton1.Checked) { c = (ClassLibrary1.Culculated.Culc)new ClassLibrary1.Culculated.DecCulc(); }
            if (radioButton2.Checked) { c = (ClassLibrary1.Culculated.Culc)new ClassLibrary1.Culculated.DexCulc(); }
            if (radioButton3.Checked) { c = (ClassLibrary1.Culculated.Culc)new ClassLibrary1.Culculated.BinCulc(); }
            textBox1.Text= c.culc(old,textBox1.Text,opr);
            }
            opr = 0;
        }
        void butonoff() {
            button4.Enabled = true;
            button5.Enabled = false;
            button6.Enabled = false;

            button7.Enabled = false;
            button8.Enabled = false;
            button9.Enabled = false;

            button15.Enabled = false;
            button14.Enabled = false;
            button13.Enabled = false;

            button11.Enabled = false;
            button10.Enabled = false;
            button18.Enabled = false;

            button17.Enabled = false;
            button16.Enabled = false;
            button21.Enabled = false;

            button18.Enabled = false;
           
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            textBox1.Text += b.Text;
            if (textBox1.Text.StartsWith("0")) {
                textBox1.Text = textBox1.Text.Substring(1);
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            textBox1.Text = "0";
            old = "";
            opr = 0;
        }
    }
}
